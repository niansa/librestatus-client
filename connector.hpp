#ifndef CONNECTOR_H
#define CONNECTOR_H


#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>



class librestatusConnector : QObject {
    QNetworkAccessManager *mNetMan = nullptr;
    QNetworkRequest *mNetReq = nullptr;
    QByteArray mDataBuffer;
    QString serverBaseUrl;

    public:
        librestatusConnector(const QString& newServerBaseUrl) {
            // Initialise variables
            mNetMan = new QNetworkAccessManager(this);
            mNetReq = new QNetworkRequest();
            if (QUrl(newServerBaseUrl, QUrl::StrictMode).isValid()) {
                serverBaseUrl = newServerBaseUrl;
            } else {
                serverBaseUrl = "http://example.com"; // TODO: throw some exception instead
            }
            // Initialise request
            //mNetReq->setTransferTimeout(15000);
            mNetReq->setHeader(QNetworkRequest::UserAgentHeader, APPNAME);
            mNetReq->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
            mNetReq->setHeader(QNetworkRequest::ContentLengthHeader, "0");
        }

        void setToken(const QString& newToken) {
            mNetReq->setRawHeader("Token", newToken.toUtf8());
        }

        QNetworkReply *sendRequest(QString requestPath) {
            QUrl serverTestUrl();
            // Create request
            mNetReq->setUrl(serverBaseUrl + requestPath);
            // Perform the request
            return mNetMan->get(*mNetReq);
        }
};


#endif // CONNECTOR_H
