#define APPVERSION "0.1-pre"
#define APPNAME "Librestatus client v" APPVERSION
#include "ui_userList.h"
#include "ui_instanceConnect.h"
#include "ui_welcome.h"
#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <QWindow>
#include <QApplication>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "connector.hpp"



enum connectionStates {
    disconnected,
    connected
} connectionState = disconnected;


class _UILoader {
    librestatusConnector *connector;

    public:
        void instanceConnect(QMainWindow *w, bool accountExists) {
            auto *thisui = new Ui::instanceConnect;
            // Show initial UI
            thisui->setupUi(w);
            w->show();
            // Add button handlers
            w->connect(thisui->connectButton, &QPushButton::clicked, [this, w, thisui, accountExists] () {
                connector = new librestatusConnector(thisui->adressBar->text());
                // Disable input
                thisui->adressBar->setEnabled(false);
                thisui->connectButton->setEnabled(false);
                // Connect
                QNetworkReply *netReply = connector->sendRequest("/raw/qohufazogo");
                w->connect(netReply, &QNetworkReply::readyRead, [this, thisui, accountExists, netReply] () {
                    // Check if server is valid
                    if (not netReply->error() and netReply->readAll() == "This is a LibreStatus server.") { // Server is valid, continue to login or registration
                        if (accountExists) { // Initiate login
                            //instanceLogin(w);
                        } else { // Initiate registration
                            //instanceRegister(w);
                        }
                        exit(0);
                    } else { // Failed, reenable input and show faílure message
                        thisui->adressBar->setEnabled(true);
                        thisui->connectButton->setEnabled(true);
                        thisui->failureText->setText("Failed to connect! Please double-check.");
                    }
                });
            });
        }
        void welcome(QMainWindow *w) {
            auto *thisui = new Ui::welcome;
            // Show initial UI
            thisui->setupUi(w);
            w->show();
            // Make sure connection state == disconnected
            connectionState = disconnected;
            // Show version
            thisui->versionInfo->setText(APPNAME);
            // Add button handlers
            w->connect(thisui->loginButton, &QPushButton::clicked, [this, w, thisui] () {
                instanceConnect(w, true);
            });
            w->connect(thisui->registerButton, &QPushButton::clicked, [this, w, thisui] () {
                instanceConnect(w, false);
            });
        }
        void userList(QMainWindow *w) {
            auto *thisui = new Ui::userList;
            // Show initial UI
            thisui->setupUi(w);
            w->show();
            // Add button handlers
            // ...
        }
};


int main(int argc, char *argv[]) {
    // Initialise variables
    QApplication a(argc, argv);
    QMainWindow w;
    _UILoader UILoader;
    // Load initial window
    UILoader.welcome(&w);
    // Run forever
    return a.exec();
}
